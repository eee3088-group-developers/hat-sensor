<u>**SHT2x Class**</u>

**SHT2x_CheckCrc()**  
Calculates checksum for n bytes of data and compares it with the expected checksum.

input: data[]              checksum is built based on this data  
nbrOfBytes     checksum is built for n bytes of data  
checksum       expected checksum  
return: error:             CHECKSUM_ERROR = checksum does not match  
                           0              = checksum matches

**SHT2x_MeasureHM()**  
Measures humidity or temperature. This function waits for a hold master until measurement is ready or a timeout has occurred.

input:     eSHT2xMeasureType  
output:   *pMeasurand:    humidity / temperature as a raw value  
return: error

Note: timing for timeout may be changed.

**SHT2x_CalcRH()**  
Calculates the relative humidity.

input: sRH: humidity raw value (16bit scaled)  
return: pHumidity relative humidity [%RH]

**SHT2x_CalcTemperatureC()**  
Calculates temperature.

input: sT: temperature raw value (16bit scaled)  
return: temperature [˙C]

**SHT2x_Process()**  
Measures humidity with “Hold Master Mode (HM)”.


<u>**I2C_HAL Class**</u>

**I2c_Init()**  
Initialises the ports for I2C interface.

**I2c_StartCondition()**  
Write a start condition on I2C-bus.
 
input: -  
output: -  
return: -  

Note: timing (delay) may have to be changed for different microcontrollers.  
          _____  
SDA:           |_______  
          _______  
SCL:                |_____  

**I2c_StopCondition()**  
Write a stop condition on I2C-bus.

input: -  
output: -  
return: -  

Note: timing (delay) may have to be changed for different microcontrollers.  
                       ______  
SDA:  _____ |  
                _________  
SCL:  __ |  

**IIC_Nack()**  
No ACK (Acknowledgement) response.

**IIC_Ack()**  
Generates ACK response.

**IIC_Wait_Ack()**  
Waits for the response signal.

input: -  
output: -  
return: 1         failed to receive response  
        0         successfully received the response  

**I2c_WriteByte()**  
Writes a byte to I2C-bus and checks acknowledge.

input: txByte transmit byte   
output: -  
return: error  

Note: timing (delay) may have to be changed for different microcontrollers.

**I2c_ReadByte()**  
Reads a byte on I2C-bus.

input: rxByte receive byte  
output: rxByte

Note: timing (delay) may have to be changed for different microcontrollers.

<u>**main Class (EEPROM)**</u>

**SystemClock_Config()**  
System clock configuration that initialises the RCC oscillators according to the specified parameters in the RCC_OscInitTypeDef structure and initialises the CPU, AHB & APB bus clocks.

input: -  
output: -  
return: -  

**MX_GPIO_Init()**  
GPIO initialization function that enables the clock and configures the GPIO pin to an output pin and configures it.

input: -  
output: -  
return: -

**MX_USART2_UART_Init()**  
USART2 initialization function.

input: -  
output: -  
return: -

**MX_I2C1_Init()**  
I2C1 initialization function that configures the analogue and digital filters.

input: -  
output: -  
return: -

**debugPrintln()**  
Sends a char array over to the UART and automatically sends a new line character after it.
