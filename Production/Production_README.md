This document contains information regarding the budgeting for the project and potential manufacturers for the HAT sensor PCB.

**Budgeting:**  
All information regarding the budgeting for the project can be found in the Bill Of Materials in the PCB/main/JLCPCB/assembly folder.

**Manufacturers:**  
JLCPCB was chosen as the manufacturers for this project. Other manufacturers would be acceptable to use.
