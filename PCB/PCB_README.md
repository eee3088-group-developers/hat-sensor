## PCB Notes
The PCB layout can be found in the main folder titled "main.kicad_pcb".\
The Main page for the Schematics can be found in the main folder titled "main.kicad_sch"

If there are any errors while attempting to open the schematics then open the "main.kicad_pro" file. From there you can manoeuvre accordingly (Schematics or pcb). 


## Notes on file type:
*.kicad_pro is the project file which allows easy accessing to both schematics and pcb.\
*.kicad_sch contains the schematic files.\
*.kicad_pcb contains the pcb file.
