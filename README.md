# Digital and Analogue Sensing project using a HAT

The following project is a Third Year Electrical Engineering design project at the University of Cape Town.
The project required the construction of a combination of sensors: a digital sensor (temperature & humidity) and an analogue sensor (battery voltage). 

The PCB schematics and gerbers were generated using KiCad software. The KiCad classes as well as the generated bill of materials can be found in the PCB folder above.   

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/eee3088-group-developers/hat-sensor.git
git branch -M main
git push -uf origin main
```

## Name
Digital and Analogue Sensing project using a HAT configured specifically for the STM32F0X series microcontroller discovery kit.

## Description
Our project makes use of a temperature and humidity sensor. This combination of sensors will detect the temperature and humidity of a given environment and display relevant information to the user. 

Schematics can be found in the PCB>main Folder along with the PCB layout. The Bill of Materials can be found in the PCB folder. 

## Usage
This system can be used in various scenarios, such as in a greenhouse for monitoring optimal conditions for plant growth, for everyday civilian use where a person can monitor temperature and humidity to make lifestyle decisions (i.e. attire/ day’s activity) and for a home or building to monitor temperature and humidity levels associated with a fire and consequently issuing a warning, alarming the homeowner to the presence of a possible fire. 

In the case of the greenhouse scenario, our HAT sensor will be mounted to a beam and will sit at a similar height to the plants to obtain the most accurate ambient condition readings. In order to detect temperature and humidity for weather monitoring, the HAT will be positioned outside in an easily accessible location. For the final scenario, the HAT will be positioned in the centre of a room, on the ceiling.

## Hardware & Software Required 
The user would need the following hardware: STM32F0 Discovery board, a power supply/ battery, a laptop or USB accepting device with a display and possibly a mounting mechanism. 

The user would need the following software: STM32Cube IDE to code the STM32F051R8T6 Discovery board, Kicad software to view the schematics and pcb, and Gitlab to access the git repo.

## How to connect the hardware? 
- In the current version (V0.2) the battery holder has not been populated and therefore power would need to be supplied via the positive and negative terminals of the battery holder. However in future versions there will be a battery holder in which case a battery would be inserted to power the board.
- The STM32F051R8T6 Discovery board would be placed on top of the HAT such that an electrical connection between the discovery board and HAT sensor is made. Ensure that the orientation of the HAT is correct in terms of the pin connections to the STM32F051R8T6 Discovery board (i.e. USB ports of the STM and HAT sensor are on the same side)

## Basic Start-up
- Connect the hardware as described above
- Download the code from the git repository and launch it in STM32Cube IDE
- Connect the STM32F051R8T6 discovery board to the computer via the USB port.
- Run the code to configure the STM32F051R8T6 discovery board - an output of temperature, humidity and battery voltage (sensor outputs) should be displayed.


## Support
For any support related queries to this project, email Ryan Silverman at SLVRYA001@myUCT.ac.za
Make the subject of the email:
"EEE3088S HAT Project GIT Support Request"

## Roadmap
The Roadmap will be updated when the first version of the PCB is developed and tested.

## Contributing
Contributions are welcome! Details for contribution are in the contributions.md 

## Authors and acknowledgment
The following people are responsible for the production and development of this project:
Ryan Silverman
Jason Peyron
Natasha Soldin

## License
This project is licensed under the Creative Commons Legal Code. The full license can be found in the main folder entitled LICENSE.

## Project status
The current status for this project is: Under development/testing.

